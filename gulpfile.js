const gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    LessAutoprefix = require('less-plugin-autoprefix'),
    LessPluginCleanCSS = require('less-plugin-clean-css'),
    autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] }),
    cleanCSSPlugin = new LessPluginCleanCSS({advanced: true}),
    sourcemaps = require('gulp-sourcemaps'),
    minify = require('gulp-minify'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    path = require('path'),
    del = require('del'),
    imagemin = require('gulp-imagemin')
;

// Проверка ошибок в скриптах
// gulp.task('lint', function() {
// return gulp.src(['./js/*.js'])
//     .pipe(jshint())
//     .pipe(jshint.reporter('default'));
// });

const cssFiles = [
    './src/css/main.less',
    './src/css/media.less'
];

const jsFiles = [
    './src/js/main.js'
];

const imgFiles = [
    './src/images/**'
];

function styles(done) {
    gulp.src(cssFiles)
        .pipe(sourcemaps.init())
        .pipe(less({
            paths: [ path.join(__dirname, 'build', 'css') ],
            plugins: [
                autoprefix,
                cleanCSSPlugin
            ]
        }))
        .pipe(concat('styles.css'))
        .pipe(rename({ suffix: '.min'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());
    done();
}

function scripts(done) {
    gulp.src(jsFiles)
        .pipe(concat('script.js'))
        .pipe(minify({
            ext:{
                min:'.min.js'
            },
            compress: true
        }))
        .pipe(gulp.dest('./build/js'))
        .pipe(browserSync.stream());
    done();
}

function imageCompress(done) {
    gulp.src(imgFiles)
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('./build/images'));
    done();
}

function sync(done) {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 3003
    });
    done();
}

function watchPrint(){
    gulp.watch("./src/css/**/*.less", styles);
    gulp.watch("./src/images/**", imageCompress);
    // gulp.watch("./src/js/**/*.js", scripts); //TODO раскоментировать если будут js скрипты на сайте
    gulp.watch("./*.html").on('change', reload);
}

async function clean() {
    const deletedPaths = await del(['build/*']);
    console.log('Deleted files and directories:\n', deletedPaths.join('\n'));
}

gulp.task('clean', clean);

gulp.task('scripts', scripts);
gulp.task('styles', styles);
gulp.task('watch', watchPrint);

gulp.task('build', gulp.series(clean, gulp.parallel(styles, imageCompress, sync))); //TODO добавить функцию scripts() если будут js скрипты на сайте
gulp.task('dev', gulp.series('build','watch'));

gulp.task('default', gulp.series('dev'));
